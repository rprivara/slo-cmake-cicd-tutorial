#include <TFile.h>

#ifndef ROOT_FILE_WRAPPER
#define ROOT_FILE_WRAPPER

/**
 * @brief A class describing a generic ROOT file wrapper
 */
class RootFileWrapper {
    public:
        /**
         * @brief Construct the ROOT file wrapper object
         */
        RootFileWrapper();

    private:
        TFile* file_;
};
#endif // ROOT_FILE_WRAPPER
