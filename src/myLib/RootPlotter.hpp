#include <TH1D.h>
#include <TGraph.h>

#include "RootFileWrapper.hpp"

#ifndef ROOT_PLOTTER
#define ROOT_PLOTTER

/**
 * @brief A class for plotting of ROOT objects
 */
class RootPlotter {
    public:
        /**
         * @brief Construct the ROOT plotter object
         * @param wrapper Root file wrapper containing some plots
         */
        RootPlotter(RootFileWrapper* wrapper);

        /**
         * @brief Dummy plot method
         */
        void Plot() {}

    private:
        TH1D random_hist_;
        TGraph random_graph_;
};
#endif // ROOT_PLOTTER
