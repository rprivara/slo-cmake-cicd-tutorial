# slo-cmake-cicd-tutorial

A simple tutorial project explaining the basic functionality of CMake and integration of unit tests.

## Source code
The project has a very simple C++ source code that utilizes `ROOT` libraries. There is no functionality, this is basically a dummy code.

## Guides
The `guides/` directory contains pdf guides for setting up a project with CMake and for configuring the repository to include CI/CD testing.

## CMake configuration
The primary CMake configuration file is `CMakeLists.txt` in the root directory. Additional configuration file is located in `src/myLib/CMakeLists.txt` and contains settings for the `myLib` subpackage library.

## CI/CD configuration
CI/CD is configured in `.gitlab-ci.yml` file. The CI/CD pipeline utilizes a setup script located in `.ci/` for sourcing `ROOT` in testing environments.
