#include <iostream>

#include "RootFileWrapper.hpp"
#include "RootPlotter.hpp"

int main(int argc, char* argv[]) {
    // Print usage if an argument was passed
    if (argc > 1) {
        std::cout << "Usage:  ./Tutorial" << std::endl
            << "\t(no arguments allowed)" << std::endl;
        
        return 0;
    }

    // Create a wrapper
    auto wrapper = new RootFileWrapper();

    // Create a plotter
    auto plotter = new RootPlotter(wrapper);
    
    // Plot something
    plotter->Plot();

    return 0;
}
